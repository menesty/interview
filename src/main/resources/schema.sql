DROP TABLE  IF EXISTS `user` CASCADE;
DROP TABLE  IF EXISTS `role_permission` CASCADE;
DROP TABLE  IF EXISTS `role` CASCADE;

CREATE TABLE `role` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL UNIQUE,
    PRIMARY KEY (`id`));

CREATE TABLE `role_permission` (
    `role_id` BIGINT(20) NOT NULL,
    `permission_name` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`role_id`, `permission_name`),
    CONSTRAINT `role_fk`
    FOREIGN KEY (`role_id`)
    REFERENCES `role` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `user` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `userName` VARCHAR(50) NOT NULL UNIQUE,
    `password` VARCHAR(100) NOT NULL,
    `role_id` BIGINT(20) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `role_id_fk`
    FOREIGN KEY (`role_id`)
    REFERENCES `role` (`id`)
    ON UPDATE CASCADE);