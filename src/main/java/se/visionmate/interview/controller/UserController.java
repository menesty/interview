package se.visionmate.interview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import se.visionmate.interview.dto.UserCreateDto;
import se.visionmate.interview.dto.UserDto;
import se.visionmate.interview.service.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {
  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('ADMIN', 'LIST_USERS')")
  @GetMapping("/v1/vis-test/users")
  public List<UserDto> getUsers() {
    return userService.getList().stream().map(UserDto::valueOf).collect(Collectors.toList());
  }

  @PreAuthorize("hasAnyAuthority('ADMIN', 'CREATE_USERS')")
  @PostMapping("/v1/vis-test/users")
  public UserDto createUser(@Valid @RequestBody UserCreateDto model) {
    return UserDto.valueOf(userService.create(model));
  }

  @PreAuthorize("hasAnyAuthority('ADMIN', 'EDIT_USERS')")
  @PutMapping("/v1/vis-test/users/{id}")
  public UserDto update(@PathVariable long id, @RequestBody @Valid UserCreateDto model) {
    return UserDto.valueOf(userService.updateById(id, model));
  }

  @PreAuthorize("hasAnyAuthority('ADMIN', 'DELETE_USERS')")
  @DeleteMapping("/v1/vis-test/users/{id}")
  public void delete(@PathVariable long id) {
    userService.delete(id);
  }
}
