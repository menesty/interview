package se.visionmate.interview.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import se.visionmate.interview.dto.RoleDto;
import se.visionmate.interview.persistance.domain.Role;
import se.visionmate.interview.service.RoleService;

import java.util.List;

@RestController
public class RoleController {
  private final RoleService roleService;

  public RoleController(RoleService roleService) {
    this.roleService = roleService;
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @GetMapping("/v1/vis-test/roles")
  public List<Role> getList() {
    return roleService.getList();
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @PostMapping("/v1/vis-test/roles")
  public Role create(@RequestBody RoleDto model) {
    return roleService.create(model);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @PutMapping("/v1/vis-test/roles/{id}")
  public Role update(@PathVariable long id, @RequestBody RoleDto model) {
    return roleService.update(id, model);
  }

  @PreAuthorize("hasAuthority('ADMIN')")
  @DeleteMapping("/v1/vis-test/roles/{id}")
  public void delete(@PathVariable long id) {
    roleService.deleteById(id);
  }
}
