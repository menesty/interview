package se.visionmate.interview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import se.visionmate.interview.dto.JwtTokenResponse;
import se.visionmate.interview.dto.LoginRequest;
import se.visionmate.interview.service.JwtTokenService;

@RestController
public class AuthController {
  private final AuthenticationManager authenticationManager;
  private final JwtTokenService jwtTokenService;

  @Autowired
  public AuthController(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenService = jwtTokenService;
  }

  @PreAuthorize("isAnonymous()")
  @PostMapping("/v1/vis-test/login")
  public JwtTokenResponse login(@RequestBody LoginRequest loginRequest) {
    Authentication authentication = authenticationManager.
            authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    final String token = jwtTokenService.generate(authentication);

    return new JwtTokenResponse(token);
  }
}
