package se.visionmate.interview.persistance.repository;

import se.visionmate.interview.persistance.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  User findByUsername(String userName);
}
