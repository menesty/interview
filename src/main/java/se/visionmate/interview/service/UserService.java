package se.visionmate.interview.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import se.visionmate.interview.dto.UserCreateDto;
import se.visionmate.interview.persistance.domain.User;

import java.util.List;

public interface UserService extends UserDetailsService {
  List<User> getList();

  User create(UserCreateDto model);

  User updateById(long id, UserCreateDto model);

  void delete(long id);
}
