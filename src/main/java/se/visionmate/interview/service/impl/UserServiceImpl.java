package se.visionmate.interview.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se.visionmate.interview.dto.UserCreateDto;
import se.visionmate.interview.exception.RoleNotFoundException;
import se.visionmate.interview.exception.UserNotFoundException;
import se.visionmate.interview.persistance.domain.Role;
import se.visionmate.interview.persistance.domain.User;
import se.visionmate.interview.persistance.repository.UserRepository;
import se.visionmate.interview.service.RoleService;
import se.visionmate.interview.service.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
  private final UserRepository userRepository;
  private final RoleService roleService;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserServiceImpl(UserRepository userRepository, RoleService roleService, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.roleService = roleService;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public List<User> getList() {
    return userRepository.findAll();
  }

  @Override
  public User create(UserCreateDto model) {
    Role role = roleService.getById(model.getRoleId()).orElseThrow(RoleNotFoundException::new);

    User user = new User();
    user.setUsername(model.getUsername());
    user.setPassword(passwordEncoder.encode(model.getPassword()));

    user.setRole(role);

    return userRepository.save(user);
  }

  @Override
  public User updateById(long id, UserCreateDto model) {
    Role role = roleService.getById(model.getRoleId()).orElseThrow(RoleNotFoundException::new);

    User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    user.setUsername(model.getUsername());
    user.setRole(role);

    return user;
  }

  @Override
  public void delete(long id) {
    userRepository.deleteById(id);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    return Optional.ofNullable(userRepository.findByUsername(username))
            .map(user -> new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getRole().getPermissions()))
            .orElseThrow(() -> new UsernameNotFoundException(String.format("User: %s, not found", username)));
  }
}
