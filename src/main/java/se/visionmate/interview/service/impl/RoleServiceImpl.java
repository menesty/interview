package se.visionmate.interview.service.impl;

import org.springframework.stereotype.Service;
import se.visionmate.interview.dto.RoleDto;
import se.visionmate.interview.exception.RoleNotFoundException;
import se.visionmate.interview.persistance.domain.Role;
import se.visionmate.interview.persistance.repository.RoleRepository;
import se.visionmate.interview.service.RoleService;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
  private final RoleRepository roleRepository;

  public RoleServiceImpl(RoleRepository roleRepository) {
    this.roleRepository = roleRepository;
  }

  @Override
  public Optional<Role> getById(long roleId) {
    return roleRepository.findById(roleId);
  }

  @Override
  public List<Role> getList() {
    return roleRepository.findAll();
  }

  @Override
  public Role create(RoleDto model) {
    Role role = new Role();

    role.setName(model.getName());
    role.setPermissions(model.getPermissions());

    return roleRepository.save(role);
  }

  @Override
  public Role update(long id, RoleDto model) {
    Role role = roleRepository.findById(id).orElseThrow(RoleNotFoundException::new);
    role.setName(model.getName());
    role.setPermissions(model.getPermissions());

    return roleRepository.saveAndFlush(role);
  }

  @Override
  public void deleteById(long id) {
    roleRepository.deleteById(id);
  }
}
