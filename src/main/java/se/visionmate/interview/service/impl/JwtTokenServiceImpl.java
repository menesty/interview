package se.visionmate.interview.service.impl;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import se.visionmate.interview.exception.TokenExpiredException;
import se.visionmate.interview.service.JwtTokenService;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class JwtTokenServiceImpl implements JwtTokenService {
  @Value("${secret.jwt}")
  private String jwtSecret;
  @Value("${secret.jwt-issuer}")
  private String jwtIssuer;
  private static final String PERMISSIONS_KEY = "permissions";
  private static final int EXPIRE_PERIOD = 2 * 24 * 60 * 60 * 1000;

  private final Logger logger = LoggerFactory.getLogger(JwtTokenServiceImpl.class);

  @Override
  public String generate(Authentication authentication) {
    return Jwts.builder()
            .setSubject(authentication.getPrincipal().toString())
            .setIssuer(jwtIssuer)
            .addClaims(Map.of(PERMISSIONS_KEY, authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())))
            .setIssuedAt(new Date())
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRE_PERIOD))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact();
  }

  public String getUsername(String token) {
    Claims claims = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .getBody();

    return claims.getSubject();
  }

  @Override
  @SuppressWarnings("unchecked")
  public Collection<String> getAuthorities(String token) {
    Claims claims = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .getBody();
    return (Collection<String>) claims.get(PERMISSIONS_KEY);
  }

  public boolean isValid(String token) {
    try {
      Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
      return true;
    } catch (MalformedJwtException ex) {
      logger.error("Invalid JWT token - {}", ex.getMessage());
    } catch (ExpiredJwtException ex) {
      logger.error("Expired JWT token - {}", ex.getMessage());
      throw new TokenExpiredException();
    } catch (UnsupportedJwtException ex) {
      logger.error("Unsupported JWT token - {}", ex.getMessage());
    } catch (IllegalArgumentException ex) {
      logger.error("JWT claims string is empty - {}", ex.getMessage());
    }
    return false;
  }
}
