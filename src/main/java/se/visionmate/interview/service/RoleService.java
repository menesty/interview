package se.visionmate.interview.service;

import se.visionmate.interview.dto.RoleDto;
import se.visionmate.interview.persistance.domain.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {
  Optional<Role> getById(long roleId);

  List<Role> getList();

  Role create(RoleDto model);

  Role update(long id, RoleDto model);

  void deleteById(long id);
}
