package se.visionmate.interview.service;

import org.springframework.security.core.Authentication;

import java.util.Collection;

public interface JwtTokenService {
  boolean isValid(String token);

  String getUsername(String token);

  Collection<String> getAuthorities(String token);

  String generate(Authentication user);
}
