package se.visionmate.interview.config.secure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import se.visionmate.interview.exception.TokenExpiredException;
import se.visionmate.interview.service.JwtTokenService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class JwtTokenPerRequestFilter extends OncePerRequestFilter {
  private JwtTokenService jwtTokenService;
  private static final String BEARER = "Bearer ";

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
          throws ServletException, IOException {
    final String requestTokenHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

    String username = null;
    Collection<String> authorities = List.of();
    String token;

    if (requestTokenHeader != null && requestTokenHeader.startsWith(BEARER)) {
      token = requestTokenHeader.substring(BEARER.length());
      try {
        if (jwtTokenService.isValid(token)) {
          username = jwtTokenService.getUsername(token);
          authorities = jwtTokenService.getAuthorities(token);
        }
      } catch (IllegalArgumentException e) {
        logger.warn("Unable to get token");
      } catch (TokenExpiredException e) {
        logger.warn("Token has expired");
      }
    } else {
      logger.warn("Authorization header do not contain Bearer String");
    }

    if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
      UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
              username, null, authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

      usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

      SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }
    chain.doFilter(request, response);
  }

  @Autowired
  public void setJwtTokenService(JwtTokenService jwtTokenService) {
    this.jwtTokenService = jwtTokenService;
  }
}