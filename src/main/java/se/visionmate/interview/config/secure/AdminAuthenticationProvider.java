package se.visionmate.interview.config.secure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class AdminAuthenticationProvider implements AuthenticationProvider {
  @Value("${secret.username}")
  private String adminUserName;
  @Value("${secret.password}")
  private String adminPassword;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = authentication.getName();
    String password = authentication.getCredentials()
            .toString();

    if (Objects.equals(adminUserName, username) && Objects.equals(adminPassword, password)) {
      return new UsernamePasswordAuthenticationToken(username, password, List.of(new SimpleGrantedAuthority("ADMIN")));
    } else {
      throw new BadCredentialsException("Incorrect credentials");
    }
  }

  @Override
  public boolean supports(Class<?> auth) {
    return auth.equals(UsernamePasswordAuthenticationToken.class);
  }
}
