package se.visionmate.interview.dto;

import se.visionmate.interview.persistance.domain.Permission;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class RoleDto {
  @NotNull
  private String name;
  private Set<Permission> permissions;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<Permission> getPermissions() {
    return permissions;
  }

  public void setPermissions(Set<Permission> permissions) {
    this.permissions = permissions;
  }
}
