package se.visionmate.interview.dto;

import se.visionmate.interview.persistance.domain.Role;
import se.visionmate.interview.persistance.domain.User;

public class UserDto {
  private long id;
  private String username;
  private Role role;

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public static UserDto valueOf(User user) {
    UserDto model = new UserDto();

    model.setUsername(user.getUsername());
    model.setId(user.getId());
    model.setRole(user.getRole());

    return model;
  }
}
