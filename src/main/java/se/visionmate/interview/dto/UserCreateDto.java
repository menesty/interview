package se.visionmate.interview.dto;

import javax.validation.constraints.NotNull;

public class UserCreateDto {
  @NotNull
  private String username;
  @NotNull
  private String password;
  private long roleId;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public long getRoleId() {
    return roleId;
  }

  public void setRoleId(long roleId) {
    this.roleId = roleId;
  }
}
